# Github application

This application is used to receive the data about any github repository

## API Format

request format: `GET /repository/{owner}/{repository_name}`

```json
{
    "cloneUrl" : "ssh url used to clone repository",
    "createdAt" : "date of creation (ISO 8601 format)",
    "description" : "repository description",
    "name" : "name of the queried repository",
    "stars" : "total count of stars given to the repository"
}
```

This REST call makes [github GraphQL API](https://developer.github.com/v4/) call.

## Infrastructure & Ops

Infrastructure is made from two ubuntu 18.04 Digital Ocean droplets (primary manager and worker) set up from terraform code. Both machines are connected into (and previously provisined) docker swarm via ansible. Terraform and ansible integration is managed by [dynamic inventory](https://github.com/adammck/terraform-inventory). Additionally manager has got manual setup for [NFS](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-18-04) for storing traefik logs and configuration. Worker has got client installed manually.

## Application

Technology stack used for this application:

* Python 3 (3.6.6) + requests (2.19.1) + Jinja2 for connecting to Github API by GraphQL
* Flask (1.0.2) for API routing
* uWSGI (2.0.17) for serving application
* Docker (17.12.1-ce +) with swarm to replicate services
* Bash + python for e2e tests

## CI/CD

During creation of this project I decided to use bitbucket pipelines because I have never had an opportunity to work with this tool. Four easy steps are included (only for master branch):

* Step 1:
    * Build docker image
    * Run unit tests inside the container
    * Push image to the registry as current commit hash
* Step 2:
    * Copy configuration files to manager server
    * Remove current stacks
    * Deploy new stacks with number of replicas defined in secrets
* Step 3:
    * Run e2e tests from docker container against endpoint defined in secrets for each suitcase seperatly
    * If tests failed use last image marked as `master` to rollback
* Step 4:
    * Mark current commit as master on docker registry

## Project structure

* `app` - contains all files necessary for application to work.
    * `.` - all configuration files
    * `code` - code of the application
        * `.` - Docker setup
        * `config` - config for pip and uwsgi
        * `data` - GraphQL requests templates
        * `tests` - tests for the service
* `deploy` - contains all files which are uploaded to the production server
    * `stack` - contains all files used by docker swarm
* `local` - contains all files that are required by local (docker-compose) setup to work
* `infra` - directory with digitalocean infrastructure wrote in terraform
* `ops` - contains script for automatic setup of the infrastructure

## Service logic

### Algorithm

Traefik proxy passes to the uwsgi application running flask api. Each request to the service renders GraphQL request from jinja template supplied with `owner` and `repository_name`. Received data is later beautified to meet output standard. This JSON is finally displayed to the user

### Errors

If there is any error (e.g. user not found, or repository not found), all informations will be displayed in json list called `errors` where each object contains `message` describing what happened.

## Tests

### E2E Tests

End 2 end tests include:

* health check: to see if service after deploy returns 200 status code
* api check: ensures that there is enough of API calls left on the token
* tests for REST service response:
    * response is json
    * all keys are inside the json
    * stars are non negative
    * name is correct
    * validates clone ssh url
    * is date ISO8601


### Load tests

I decided to face requirements of the task in real environment instead of creating local POC.

Testing machine setup:

* Ubuntu 18.04
* lenovo y580
* i7 3610QM
* 12 GB RAM
* 128 GB SSD

Environment setup for tests:

* uWSGI - 4 processes per 1 thread per 1 container
* swarm - 8 replicas over 2 nodes (Digital Ocean standard droplets, ubuntu 18.04, 1 GB RAM, 1 vCPU, 25 GB SSD, 1 TB Transfer)

128 requests were sent with concurency of 32 using [ab tool](https://httpd.apache.org/docs/2.4/programs/ab.html) to check statistics of my [.dotfiles repository](https://github.com/mkjmdski/.dotfiles).

`ab -c 32 -n 128 https://github-app.mlodzikowski.pl/repository/mkjmdski/.dotfiles`

![load test with apache benchmark](loadtests.png)
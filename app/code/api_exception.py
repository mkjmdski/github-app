class APIException(Exception):
    status_code = 500

    def __init__(self, errors, status_code=None):
        super()
        self.errors = errors
        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        if self.errors is not dict:
            rv = dict()
            rv['errors'] = self.errors
            return rv
        else:
            return self.errors

from api_exception import APIException
from query_helpers import parse_query_result, run_query
from flask import Flask, jsonify
from data import github_repo_template, apicheck_request
import logging


application = Flask(__name__)
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@application.errorhandler(APIException)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    logger.error(f'response: {response}')
    return response


@application.route('/repository/<owner>/<repository>')
def display_repository_info(owner, repository):
    logger.info(f'quering: {owner}/{repository}')
    template_variables = {
        "owner": owner,
        "repository": repository}
    logger.debug('parsing query...')
    # Here it's possible to switch template depending on the backend
    parsed_query = github_repo_template.render(**template_variables)
    logger.debug(f'query: {parsed_query}')
    logger.debug('running query...')
    # Here it's possible to add logic for various backends
    result = run_query('https://api.github.com/graphql', parsed_query)
    if 'errors' in result:
        logger.debug(f'github api call result: {result}')
        errors = []
        for error in result['errors']:
            errors.append({'message': error['message']})
        raise APIException(errors)
    else:
        parsed_result = parse_query_result(result)
        logger.debug(f'parsed github api call result: {parsed_result}')
        return jsonify(parsed_result)


@application.route('/healthcheck')
def i_am_fine():
    logger.info('healthcheck')
    return 'ok', 200


@application.route('/api_calls_left')
def check_api_calls_left():
    logger.info('checking token')
    result = run_query(apicheck_request)
    logger.debug(f'api calls left result: {result}')
    return jsonify(result)

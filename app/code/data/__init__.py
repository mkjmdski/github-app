from jinja2 import Environment, FileSystemLoader
import os


script_location = os.path.dirname(os.path.realpath(__file__))
jinja_loader = FileSystemLoader('data')
j2_env = Environment(loader=jinja_loader)
github_repo_template = j2_env.get_template('github.graphql.jinja2')
apicheck_request = open(f'{script_location}/apicheck.graphql').read()

import requests
from api_exception import APIException
from os import getenv


def run_query(endpoint, query):
    request = requests.post(
        endpoint,
        json={'query': query},
        headers={"Authorization": f'token {getenv("GITHUB_TOKEN")}'})
    if request.status_code == 200:
        return request.json()
    else:
        raise APIException(
            {"errors": {"0": {
                "message": "status code for query is not 200",
                "query": query}}},
            status_code=request.status_code)


def parse_query_result(result):
    result = result['data']['repository']
    result['stars'] = result['stargazers']['totalCount']
    result['cloneUrl'] = result['sshUrl']
    result.pop('sshUrl')
    result.pop('stargazers')
    return result

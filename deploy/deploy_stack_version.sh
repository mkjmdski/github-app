#!/bin/bash -x
docker stack rm github-app
docker stack rm traefik
docker stack deploy --compose-file /app/data/stack/traefik.yaml traefik
docker stack deploy --compose-file /app/data/stack/app.yaml github-app
import unittest
import datetime
import requests
import logging
from urllib.parse import urlparse
from json import JSONDecodeError
from os import getenv

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('E2E Tests')


class TestEndpointHealth(unittest.TestCase):

    endpoint = ''
    owner = ''
    repo = ''
    response = {}

    def test_health_check_returns_200(self):
        status_code = requests.get(f"{self.endpoint}/healthcheck").status_code
        error_message = f"Healthcheck failed: {status_code}"
        self.assertEqual(status_code, 200, msg=error_message)
        logger.info('Healthcheck: ok')

    def test_token_has_got_more_than_0_calls(self):
        response = requests.get(f"{self.endpoint}/api_calls_left").json()
        api_calls_left = response['data']['rateLimit']['remaining']
        error_message = f"Token is not healthy, calls lef: {api_calls_left}"
        self.assertGreater(api_calls_left, 0, msg=error_message)
        logger.info('API Token: healthy')

    def test_clone_url_matches_owner_and_repo(self):
        error_message = "Clone url doesn't match owner and/or repository"
        received_url = str(self.response['cloneUrl']).lower()
        repo_format = f"{self.owner.lower()}/{self.repo.lower()}"
        correct_url = f"git@github.com:{repo_format}.git"
        self.assertEqual(
            received_url, correct_url,
            msg=f"{error_message} {received_url}")
        logger.info("Clone url is valid")

    def test_stars_should_be_non_negative(self):
        error_message = "Star gazers are not greater than 0:"
        self.assertGreaterEqual(
            self.response['stars'], 0,
            msg=f"{error_message} {self.response['stars']}")
        logger.info("Star gazers are greater or equal 0")

    def test_repo_name_should_be_as_in_query(self):
        error_message = "Repository name doesn't match query"
        self.assertEqual(
            self.response['name'].lower(), self.repo.lower(),
            msg=f"{error_message} {self.response['name'].lower()}")
        logger.info("Repo name matches the query")

    def test_date_should_be_iso8601(self):
        try:
            datetime.datetime.strptime(
                self.response['createdAt'], '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            self.fail(f"{self.response['createdAt']} is not ISO_8601")
        logger.info("Date is ISO_8601")

    def test_response_should_have_all_keys(self):
        for key in ['name', 'createdAt', 'cloneUrl', 'description', 'stars']:
            self.assertIn(key, self.response, msg=f"response misses {key}")
        logger.info("Response contains all keys")

    def set_response(self):
        try:
            response = requests.get(
                f"{self.endpoint}/repository/{self.owner}/{self.repo}")
            self.response = response.json()
        except JSONDecodeError:
            self.fail("Response is not valid json")

    def setUp(self):
        self.endpoint = getenv('ENDPOINT')
        self.owner = getenv('OWNER')
        self.repo = getenv('REPOSITORY')
        self.check_variables()
        self.check_endpoint()
        self.set_response()

    def check_variables(self):
        if not self.endpoint:
            self.fail("You forget to specify env ENDPOINT")
        if not self.owner:
            self.fail("You forget to specify env OWNER")
        if not self.repo:
            self.fail("You forget to specify env REPOSITORY")

    def check_endpoint(self):
        parsed_endpoint = urlparse(self.endpoint)
        self.assertEqual(parsed_endpoint[0], 'https')


if __name__ == "__main__":
    unittest.main()

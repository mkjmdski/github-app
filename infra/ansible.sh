#!/usr/bin/env bash
declare root="$(git rev-parse --show-toplevel)"
TF_STATE="${root}/terraform/terraform.tfstate" \
    ansible-playbook \
    --inventory-file="$(which terraform-inventory)" \
    "${root}/ansible/playbook.yaml"
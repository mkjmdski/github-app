provider "digitalocean" {
    token = "${var.do_token}"
}
resource "digitalocean_ssh_key" "local" {
    name       = "Mikolaj Personal"
    public_key = "${file("~/.ssh/id_rsa.pub")}"
}
resource "digitalocean_droplet" "docker_swarm_primary_manager" {
    image = "ubuntu-18-04-x64"
    region = "fra1"
    size = "s-1vcpu-1gb"
    name = "${var.project_name}-primary-manager"
    ssh_keys = ["${digitalocean_ssh_key.local.id}"]
}

# resource "digitalocean_droplet" "docker_swarm_manager" {
#     count = "2"
#     image = "ubuntu-18-04-x64"
#     region = "fra1"
#     size = "s-1vcpu-1gb"
#     name = "${var.project_name}-manager-${count.index}"
#     ssh_keys = ["${digitalocean_ssh_key.local.id}"]
# }

resource "digitalocean_droplet" "docker_swarm_worker" {
    count = "1"
    image = "ubuntu-18-04-x64"
    region = "fra1"
    size = "s-1vcpu-1gb"
    name = "${var.project_name}-worker-${count.index}"
    ssh_keys = ["${digitalocean_ssh_key.local.id}"]
}
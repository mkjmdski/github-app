variable "do_token" {
    type = "string"
}
variable "project_name" {
    type = "string"
    default = "example-project"
}